class CreateLaptops < ActiveRecord::Migration
  def change
    create_table :laptops do |t|
      t.string :model, null: false
      t.integer :hdsize, null: false
      t.boolean :hdssd, null: false
      # t.integer :cpu_rating, null: false
      t.integer :battery_life, null: false
      t.boolean :dedicated_gpu, default: false, null: false
      t.integer :monitor_width, null: false
      t.integer :monitor_length, null: false
      t.integer :resolution, null: false
      t.integer :weight, null: false
      t.integer :ram, null: false
      t.integer :gpu_rating
      t.integer :price, null: false
      t.string :laptop_type, null: false
      t.string :amazon_url
      t.timestamps
    end
    add_index :laptops, :laptop_type
    add_index :laptops, :amazon_url, unique: true
    add_index :laptops, :price
    add_index :laptops, :monitor_width
    add_index :laptops, :monitor_length
    add_index :laptops, :resolution
    add_index :laptops, :battery_life
  end
end
