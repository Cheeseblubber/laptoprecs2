# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Laptop.create!(
  model: "Asus",
  hdsize: 1000,
  hdssd: false,
  battery_life: 4,
  dedicated_gpu: false,
  monitor_width: 15,
  monitor_length: 16,
  resolution: 768,
  weight: 6,
  ram: 4,
  price: 400,
  laptop_type: "low performance",
)
Laptop.create!(
  model: "Mac book pro",
  hdsize: 500,
  hdssd: true,
  battery_life: 6,
  dedicated_gpu: false,
  monitor_width: 15,
  monitor_length: 16,
  resolution: 1080,
  weight: 3,
  ram: 4,
  price: 1500,
  laptop_type: "high performance",
)

Laptop.create!(
  model: "Lenovo",
  hdsize: 500,
  hdssd: false,
  battery_life: 4,
  dedicated_gpu: false,
  monitor_width: 15,
  monitor_length: 16,
  resolution: 720,
  weight: 4,
  ram: 8,
  price: 800,
  laptop_type: "mid performance",
)
