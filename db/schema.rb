# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140511090621) do

  create_table "laptops", force: true do |t|
    t.string   "model",                          null: false
    t.integer  "hdsize",                         null: false
    t.boolean  "hdssd",                          null: false
    t.integer  "battery_life",                   null: false
    t.boolean  "dedicated_gpu",  default: false, null: false
    t.integer  "monitor_width",                  null: false
    t.integer  "monitor_length",                 null: false
    t.integer  "resolution",                     null: false
    t.integer  "weight",                         null: false
    t.integer  "ram",                            null: false
    t.integer  "gpu_rating"
    t.integer  "price",                          null: false
    t.string   "laptop_type",                    null: false
    t.string   "amazon_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "laptops", ["amazon_url"], name: "index_laptops_on_amazon_url", unique: true
  add_index "laptops", ["battery_life"], name: "index_laptops_on_battery_life"
  add_index "laptops", ["laptop_type"], name: "index_laptops_on_laptop_type"
  add_index "laptops", ["monitor_length"], name: "index_laptops_on_monitor_length"
  add_index "laptops", ["monitor_width"], name: "index_laptops_on_monitor_width"
  add_index "laptops", ["price"], name: "index_laptops_on_price"
  add_index "laptops", ["resolution"], name: "index_laptops_on_resolution"

end
