class LaptopsController < ApplicationController

  def index
    @laptops
  end

  private

  def laptop_params
    params.require(:laptop).permit(
    :model,
    :hdsize,
    :hdssd,
    :battery_life,
    :dedicated_gpu,
    :monitor_width,
    :monitor_lenght,
    :resolution,
    :weight,
    :ram,
    :gpu_rating,
    :price,
    :laptop_type,
    :amazon_url
   )
  end

end
