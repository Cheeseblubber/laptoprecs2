# == Schema Information
#
# Table name: laptops
#
#  id             :integer          not null, primary key
#  model          :string(255)      not null
#  hdsize         :integer          not null
#  hdssd          :boolean          not null
#  battery_life   :integer          not null
#  dedicated_gpu  :boolean          default(FALSE), not null
#  monitor_width  :integer          not null
#  monitor_length :integer          not null
#  resolution     :integer          not null
#  weight         :integer          not null
#  ram            :integer          not null
#  gpu_rating     :integer
#  price          :integer          not null
#  laptop_type    :string(255)      not null
#  amazon_url     :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#

class Laptop < ActiveRecord::Base

  validates( :model,
    :hdsize,
    :battery_life,
    :monitor_width,
    :monitor_length,
    :resolution,
    :weight,
    :ram,
    :price,
    :laptop_type,
    presence: true
  )

  def self.portable_laptops
    Laptop.where("weight < 4 AND monitor_width < 14")
  end

  def self.high_performance
    Laptop.where(laptop_type: "high performance")
  end

  def self.low_performance
    Laptop.where(laptop_type: "low performance")
  end

  def self.mid_performance
    Laptop.where(laptop_type: "mid performance")
  end

  def self.long_battery_life
    Laptop.where("battery_life > 10")
  end

  def self.mid_battery_life
    Laptop.where("battery_life BETWEEN 6 AND 10")
  end

  def self.low_battery_life
    Laptop.where("battery_life < 5")
  end

  def self.price_range(min_price = 0, max_price)
    Laptop.where("price BETWEEN ? AND ?", min_price, max_price)
  end

  def self.high_resolution
    Laptop.where("resolution >= 1080")
  end

end
